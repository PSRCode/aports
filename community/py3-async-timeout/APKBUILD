# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Contributor: Antoine Fontaine <antoine.fontaine@epfl.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-async-timeout
_pkgname=async-timeout
pkgver=4.0.0
pkgrel=0
pkgdesc="Timeout context manager for asyncio programs"
url="https://pypi.python.org/pypi/async_timeout"
arch="noarch"
license="Apache-2.0"
depends="python3 py3-typing-extensions"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-asyncio"
makedepends="python3-dev py3-setuptools"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest
}

package() {
	python3 setup.py install --root="$pkgdir"
}

sha512sums="
92716106b42ad3746c9873649b44d414da08f75cd507a0dc45d764270bcbd6f483c149e5cebe1a4c36f5cd02397c21b67b92a0475117d9dea17c580e422cf4da  py3-async-timeout-4.0.0.tar.gz
"
