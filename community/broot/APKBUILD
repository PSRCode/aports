# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=broot
pkgver=1.6.6
pkgrel=1
pkgdesc="New way to see and navigate directory trees"
url="https://github.com/Canop/broot"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="MIT"
makedepends="cargo libgit2-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Canop/broot/archive/v$pkgver.tar.gz
	minimize-size.patch
	"

build() {
	cargo build --release --locked
}

check() {
	cargo test --locked
}

package() {
	install -Dm0755 target/release/broot "$pkgdir"/usr/bin/broot
	install -Dm0644 man/page "$pkgdir"/usr/share/man/man1/broot.1
}

sha512sums="
c1e3b375904c5589fda5950617142675a9f7c16656c954cda141b3bf32acd3bb1db0fec095bfe77f648be48865b3c7251e3b6ca94da7f5ff0f72915a3a1eb90a  broot-1.6.6.tar.gz
05bbce2c8553beba42d13e50de87e4a320aed45787d788269388e04408cf9325dba5cd44ce24c30547483e7b6e9561d3aca17e06ca2b8097fc24a387395dac0b  minimize-size.patch
"
