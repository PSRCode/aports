# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-workspace-wallpapers
pkgver=5.23.2
pkgrel=0
pkgdesc="Wallpapers for the Plasma Workspace"
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="extra-cmake-modules"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-workspace-wallpapers-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2221e6b47fbda5a93e08f5dedbab3f71dfa011c796b94a9b1a932b06406b066af1d93eeb2de7be9c75add996b4f4bb4f38a865b605a16abb1813c042af7343db  plasma-workspace-wallpapers-5.23.2.tar.xz
"
