# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-integration
pkgver=5.23.2
pkgrel=0
pkgdesc="Qt Platform Theme integration plugins for the Plasma workspaces"
# armhf blocked by qt5-qtdeclarative
# s390x, mips64 and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.1-only AND ((LGPL-2.1-only WITH Nokia-Qt-exception-1.1) OR (GPL-3.0-only WITH Nokia-Qt-exception-1.1))"
depends="
	font-noto
	qqc2-desktop-style
	ttf-hack
	"
makedepends="
	breeze-dev
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libxcursor-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtx11extras-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-integration-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontNotoSans=true \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontHack=true
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
a4cf75d7544f1a452318bbc80b4b07cc5208b73820f3ea9ca3a61de5efe73b6ab273a5be4ae5d0d4d76cc453c00eea93af29ae1a1954cd5e1c662512a16aedbd  plasma-integration-5.23.2.tar.xz
"
